## General Information

### Description
<!-- include a description of the change and which issue is fixed -->

### Demo
<!-- uncomment this if the project has review apps in use
- [ ] Review app is up to date, deployment is running or finsihed
-->

<!-- screenshots, screen recordings or link to review app -->

### Related work
- [ ] Related to: <!-- branch or MR -->
- [ ] Subsequent work: <!-- features or bugs resulted from this MR -->
- [ ] Depending on: <!-- branch that this one is depending on -->

<!-- Uncomment this seciton if feature contains a breaking change, otherwise delete it
### Breaking change
#### Breaks
feature/list of features that is/are ultimately incomaptible or not available anymore

#### Reproduction Steps
describe how the feature/s worked before, hence how you can reproduce the break

#### Fix
describe what the fix is, if there is any

#### Migration
- [ ] breaking change can be fixed with migration
- [ ] automatic migration possible
- [ ] migration requires manual steps:
    - [ ] step 1
-->

### Requirements
- [ ] Installation needed
- [ ] DB changes needed
    - [ ] Migration
    - [ ] Schema update
    - [ ] Data import
- [ ] Environment variable changes:
<!--
```
file/path

name: oldValue -> newValue
name: newValue
```
-->

## Checklists

### General checklist
- Documentation:
    - [ ] created/updated or
    - [ ] not needed
- [ ] Tests added
- [ ] Conforms to the [code review guidelines](https://docs.gitlab.com/ee/development/code_review.html)
- [ ] Conforms to the [clean code guidelines](https://gist.github.com/wojteklu/73c6914cc446146b8b533c0988cf8d29)
<!-- -->
- [ ] I have performed a self-review of my own code
- [ ] I have tested my own code
- [ ] I have regression tested the application and did not break working code
- [ ] I have commented my code, particularly in hard-to-understand areas
- [ ] My changes generate no new warnings

### Database checklist
<details>
<summary>Click here to collapse/fold</summary>

<br>

- [ ] Conforms to the [merge request performance guidelines](https://docs.gitlab.com/ee/development/merge_request_performance_guidelines.html)

When adding migrations:

- [ ] Updated the schema
- [ ] Added a `down` method so the migration can be reverted
- [ ] Tested migration of database with stage or production database dump
- [ ] Added tests for migration to verify successful data transformation

When adding or modifying queries to improve performance:

- [ ] Included data that shows the performance improvement, preferably in the form of a benchmark
- [ ] Included the output of `explain` of the relevant queries

When adding tables:

- [ ] Ordered columns based on guidelines
- [ ] Added indexes for fields that are used in statements such as WHERE, ORDER BY, GROUP BY, and JOINs

When removing columns, tables, indexes or other structures:

- [ ] Added migration
- [ ] Made sure the application (ignores or) no longer uses these structures

</details>

### UI/UX checklist
- [ ] Needs UI testing
- [ ] Needs offline testing

<details>
<summary>Click here to collapse/fold</summary>

<br>

Tested in following browsers:
<!-- reduce the list to the supported browsers and remove the offline column if not needed -->
**Desktop**

| Browser             | Online                  | Offline                |
| ------------------- | ----------------------- | ---------------------- |
| Chrome              | <ul><li>[ ] </li></ul>  | <ul><li>[ ] </li></ul> |
| Chromium            | <ul><li>[ ] </li></ul>  | <ul><li>[ ] </li></ul> |
| Firefox             | <ul><li>[ ] </li></ul>  | <ul><li>[ ] </li></ul> |
| Safari              | <ul><li>[ ] </li></ul>  | <ul><li>[ ] </li></ul> |
| Edge                | <ul><li>[ ] </li></ul>  | <ul><li>[ ] </li></ul> |
| IE 11 (Windows 10)  | <ul><li>[ ] </li></ul>  | <ul><li>[ ] </li></ul> |
| IE 11 (Windows 8.1) | <ul><li>[ ] </li></ul>  | <ul><li>[ ] </li></ul> |
| IE 11 (Windows 8)   | <ul><li>[ ] </li></ul>  | <ul><li>[ ] </li></ul> |
| IE 11 (Windows 7)   | <ul><li>[ ] </li></ul>  | <ul><li>[ ] </li></ul> |

<br>

**Mobile**

| Browser                                          | Online                  | Offline                |
| ------------------------------------------------ | ----------------------- | ---------------------- |
| iOS Chrome (from iOS version x)                  | <ul><li>[ ] </li></ul>  | <ul><li>[ ] </li></ul> |
| iOS Safari (from iOS version x)                  | <ul><li>[ ] </li></ul>  | <ul><li>[ ] </li></ul> |
| Android Chrome (from Android version x)          | <ul><li>[ ] </li></ul>  | <ul><li>[ ] </li></ul> |
| Android Firefox (from Android version x)         | <ul><li>[ ] </li></ul>  | <ul><li>[ ] </li></ul> |
| Android default browser (from Android version x) | <ul><li>[ ] </li></ul>  | <ul><li>[ ] </li></ul> |

</details>
